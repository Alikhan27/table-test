import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
import {Coverage, Rows, SetGateway} from '../../../../shared/models/TableTest';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TableTestService} from '../../../../shared/services/table-test.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-create-or-edit-table',
  templateUrl: './create-or-edit-table.component.html',
  styleUrls: ['./create-or-edit-table.component.scss']
})
export class CreateOrEditTableComponent implements OnInit, OnChanges, OnDestroy {
  _gateway: Rows;
  @Input()
  set gateway(value: { column: string; row: Rows }) {
    if (value) {
      this._gateway = Object.assign({}, value.row);
    } else {
      this._gateway = null;
    }
  };

  get selectGateway(): Rows {
    return this._gateway;
  }

  @Output() hideModal: EventEmitter<{ close: boolean; changes: boolean }> = new EventEmitter<{ close: boolean; changes: boolean }>();

  form: FormGroup = new FormGroup({
    id: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required]),
    ip: new FormControl(''),
    port: new FormControl(''),
    coverage_network_id: new FormControl('', [Validators.required]),
    description: new FormControl(''),
    disabled: new FormControl(''),
    eui: new FormControl('', [Validators.required])
  });

  selectCoverage: FormControl = new FormControl('');

  coverages: Coverage[];
  coveragesItems: string[];

  destroy$: Subject<any> = new Subject<any>();

  constructor(private tableTestService: TableTestService,
              private toast: ToastrService) {
  }

  ngOnInit(): void {
    this.getCoverages();
    this.changeCoverageId();
  }

  ngOnChanges(): void {
    this.patchFormValue();
  }

  patchFormValue(): void {
    if (this.selectGateway) {
      if (this.selectGateway.disabled === 'Выкл') {
        this.selectGateway.disabled = false;
      } else if (this.selectGateway.disabled === 'Вкл') {
        this.selectGateway.disabled = true;
      } else {
        this.selectGateway.disabled = null;
      }
      const coverage: Coverage = this.coverages.find((c: Coverage) => c.id === this.selectGateway.coverage_network.id);
      if (coverage) {
        this.selectCoverage.patchValue(coverage.title || '');
      }
      this.selectGateway.coverage_network_id = this.selectGateway.coverage_network.id;
      this.form.patchValue(this.selectGateway);
    }
  }

  getCoverages(): void {
    this.tableTestService.getCoverage().subscribe(
      (c: Coverage[]) => {
        this.coverages = c;
        this.coveragesItems = this.coverages.map((c: Coverage) => c.title);
      });
  }

  changeCoverageId(): void {
    this.selectCoverage.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(v => {
        const selectCov: Coverage = this.coverages.find((c: Coverage) => c.title === v);
        if (selectCov) {
          this.form.get('coverage_network_id').patchValue(selectCov.id);
        }
      });
  }

  close(): void {
    this.hideModal.emit({close: true, changes: false});
    this.resetForm();
  }

  resetForm(): void {
    this.form.reset();
    this.selectCoverage.reset();
  }

  createOrEdit(): void {
    let body: SetGateway = this.form.value;
    console.log(body);
    this.tableTestService.setGateway(body).subscribe();
  }

  delete(): void {
    this.tableTestService.delGateway(this.selectGateway.id).subscribe(
      () => {
        this.hideModal.emit({close: true, changes: true});
        this.resetForm();
        this.toast.success('Шлюз удален!');
      },
      e => {
        console.log(e);
      },
      () => console.log('complete del!'));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
