import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditTableComponent } from './create-or-edit-table.component';

describe('CreateOrEditTableComponent', () => {
  let component: CreateOrEditTableComponent;
  let fixture: ComponentFixture<CreateOrEditTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
