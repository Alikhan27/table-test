import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../shared/services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login-test',
  templateUrl: './login-test.component.html',
  styleUrls: ['./login-test.component.scss']
})
export class LoginTestComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    username: new FormControl('a.zhumazhanov', [Validators.required]),
    password: new FormControl('eSFqvJT2FFn2HEp', [Validators.required])
  });

  constructor(private authService: AuthService,
              private router: Router,
              private toast: ToastrService) {
  }

  ngOnInit() {
  }

  login(): void {
    this.authService.login(this.loginForm.value).subscribe(
      () => {
        this.toast.success('Вы вошли в систему!');
        this.router.navigate(['/table-test']);
      },
      e => {
        console.log(e);
        this.toast.error('Произошла ошибка на сервере!')
      },
      () => console.log('complete login-test stream!')
    );
  }
}
