import {Component, OnInit, ViewChild} from '@angular/core';
import {Columns, Rows} from '../../../../shared/models/TableTest';
import {TableTestService} from '../../../../shared/services/table-test.service';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';

@Component({
  selector: 'app-table-test',
  templateUrl: './table-test.component.html',
  styleUrls: ['./table-test.component.scss']
})
export class TableTestComponent implements OnInit {
  @ViewChild('modal') modal;

  loading: boolean = false;
  rows = [];
  data = [];
  columns: Columns[] = [
    {title: 'Название', name: 'title', sort: 'desc', filtering: {filterString: '', placeholder: 'Фильтр по названию'}},
    {title: 'Сеть покрытия', name: 'coverage_network.title', sort: 'desc'},
    {title: 'Описание', name: 'description', sort: 'desc', filtering: {filterString: '', placeholder: 'Фильтр по описанию'}},
    {title: 'EUI', name: 'eui', sort: 'desc', filtering: {filterString: '', placeholder: 'Фильтр по EUI'}},
    {title: 'Количество данных в час', name: 'data_count_per_hour', sort: 'desc'},
    {title: 'Вкл/Выкл', name: 'disabled', sort: 'desc'},
    {title: 'Последнее событие', name: 'last_seen', sort: 'desc'}
  ];
  gateway: Rows;
  config: any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered', 'mb-0', 'd-table-fixed']
  };

  itemsPerPage: number = 10;
  length: number = 0;
  maxSize: number = 5;
  numPages: number = 1;
  page: number = 1;
  none: string = '';

  constructor(private tableTestService: TableTestService,
              private toastService: ToastrService) {
    this.length = this.data.length;
  }

  ngOnInit() {
    this.getRows();
  }

  getRows(): void {
    this.loading = true;
    this.tableTestService.getGateway().subscribe(
      (rows: Rows[]) => {
        rows.forEach((r: Rows) => {
          if (r) {

            if (!r.title) {
              r.title = this.none;
            }

            if (r.coverage_network && !r.coverage_network.title) {
              r.coverage_network.title = this.none;
            } else if (!r.coverage_network) {
              r.coverage_network = {title: this.none};
            }

            if (!r.description) {
              r.description = this.none;
            }

            if (!r.eui) {
              r.eui = this.none;
            }

            if (!r.data_count_per_hour) {
              r.data_count_per_hour = 0;
            }

            if (r.disabled) {
              r.disabled = 'Вкл';
            } else {
              r.disabled = 'Выкл';
            }

            if (!r.last_seen) {
              r.last_seen = this.none;
            } else {
              const time = r.last_seen.split('.')[0];
              r.last_seen = moment(time).format('HH:mm:ss DD/MM/YYYY');
            }

          }
        });
        this.rows = rows;
        this.data = this.rows;
        this.onChangeTable(this.config);
        this.loading = false;
      },
      e => {
        console.log(e);
        this.loading = false;
        this.toastService.error('Произошла ошибка на сервере!');
      });
  }

  onChangeTable(config: any, page: any = {page: this.page, itemsPerPage: this.itemsPerPage}): any {
    if (config.filtering) {
      (<any>Object).assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      (<any>Object).assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name] && item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; ++i) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    console.log(columnName);

    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  onCellClick(gateway: Rows): void {
    console.log(gateway);
    this.gateway = gateway;
    this.modal.show();
  }

  modalAfterClose(event: { close: boolean; changes: boolean }): void {
    if (event.close) {
      this.modal.hide();
    }

    if (event.changes) {
      this.getRows();
    }
  }
}
