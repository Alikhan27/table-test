import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService) {
  }

  canActivate(): boolean {
    let login: boolean = false;

    const token = this.authService.getToken();
    if (token) {
      login = true;
    }

    return login;
  }
}
