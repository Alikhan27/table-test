import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable()
export class Interceptors implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userToken = this.authService.getToken();
    if (userToken) {
      const modifiedReq = request.clone({
        headers: request.headers.set('Authorization', `JWT ${userToken}`),
      });
      return next.handle(modifiedReq);
    } else {
      return next.handle(request);
    }
  }
}
