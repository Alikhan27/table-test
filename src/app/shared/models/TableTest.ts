export interface Columns {
  title: string;
  className?: string | string[];
  name: string;
  sort?: string | boolean;
  filtering?: {
    filterString: string;
    placeholder: string;
  };
}

export interface Rows {
  id: number;
  title: string;
  description: string;
  ip?: any;
  port?: any;
  eui: string;
  last_seen?: any;
  disabled?: boolean | string;
  coverage_network?: Coverage;
  coverage_network_id?: number;
  permissions?: {
    add: boolean;
    delete: boolean;
    change: boolean;
    view: boolean;
  };
  data_count_per_hour?: number | string;
  coverage?: {
    image: string;
    bounds: number[];
    status: {
      date: string;
      status: number;
      message: string;
    }
  };
  is_planning?: boolean;
  rtt?: any;
  additional_fields_values?: any;
}

export interface Coverage {
  id?: number;
  title: string;
  status?: {
    date: string;
    status: number;
    message: string;
  }
}

export interface SetGateway {
  id?: number | string;
  title: string;
  description: string;
  ip: number;
  port: number;
  eui: number;
  coverage_network_id: number;
  disabled: boolean;
}
