import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Coverage, Rows, SetGateway} from '../models/TableTest';

@Injectable({
  providedIn: 'root'
})
export class TableTestService {

  constructor(private http: HttpClient) {
  }

  getGateway(): Observable<Rows[]> {
    return this.http.get<Rows[]>('/api/v1/gateway/');
  }

  setGateway(body: SetGateway): Observable<any> {
    let url: string = '/api/v1/gateway/';
    if (body.id) {
      url += `${body.id}/`;
    }
    return this.http.put<any>(url, body, {observe: 'response'});
  }

  delGateway(id: number): Observable<any> {
    return this.http.post<any>('/api/v1/gateway/action_delete/', {data: [id]}, {observe: 'response'});
  }

  getCoverage(): Observable<Coverage[]> {
    return this.http.get<Coverage[]>('/api/v1/coverage/cover_list/');
  }
}
