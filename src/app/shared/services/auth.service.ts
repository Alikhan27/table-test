import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/Auth';
import {tap} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLogin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
  }

  getIsLogin(): BehaviorSubject<boolean> {
    return this.isLogin;
  }

  setIsLogin(value: boolean): void {
    this.isLogin.next(value);
  }

  login(body: User): Observable<{ token: string }> {
    return this.http.post<{ token: string }>('/api/v1/auth/login/', body)
      .pipe(
        tap((res: { token: string }) => {
          this.setIsLogin(true);
          localStorage.setItem('tableToken', res.token);
        })
      );
  }

  getToken(): string {
    return localStorage.getItem('tableToken');
  }
}
